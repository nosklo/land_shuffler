#!/usr/bin/env python
#-*- coding:utf-8 -*-

import tkinter as t
import collections
import itertools
import random

all_lands = [
    'Plains (DAR) 251',
    'Plains (DAR) 252',
    'Plains (DAR) 253',
    'Island (DAR) 255',
    'Island (DAR) 256',
    'Island (DAR) 257',
    'Swamp (DAR) 259',
    'Swamp (DAR) 260',
    'Swamp (DAR) 261',
    'Mountain (DAR) 263',
    'Mountain (DAR) 264',
    'Mountain (DAR) 265',
    'Forest (DAR) 267',
    'Forest (DAR) 268',
    'Forest (DAR) 269',
    'Plains (RIX) 192',
    'Island (RIX) 193',
    'Swamp (RIX) 194',
    'Mountain (RIX) 195',
    'Forest (RIX) 196',
    'Plains (XLN) 260',
    'Plains (XLN) 261',
    'Plains (XLN) 262',
    'Plains (XLN) 263',
    'Island (XLN) 264',
    'Island (XLN) 265',
    'Island (XLN) 266',
    'Island (XLN) 267',
    'Swamp (XLN) 268',
    'Swamp (XLN) 269',
    'Swamp (XLN) 270',
    'Swamp (XLN) 271',
    'Mountain (XLN) 272',
    'Mountain (XLN) 273',
    'Mountain (XLN) 274',
    'Mountain (XLN) 275',
    'Forest (XLN) 276',
    'Forest (XLN) 277',
    'Forest (XLN) 278',
    'Forest (XLN) 279',
    'Plains (M19) 261',
    'Plains (M19) 262',
    'Plains (M19) 263',
    'Plains (M19) 264',
    'Island (M19) 265',
    'Island (M19) 266',
    'Island (M19) 267',
    'Island (M19) 268',
    'Swamp (M19) 269',
    'Swamp (M19) 270',
    'Swamp (M19) 271',
    'Swamp (M19) 272',
    'Mountain (M19) 273',
    'Mountain (M19) 274',
    'Mountain (M19) 275',
    'Mountain (M19) 276',
    'Forest (M19) 277',
    'Forest (M19) 279',
    'Forest (M19) 280',
    'Mountain (DAR) 262',
    'Plains (DAR) 250',
    'Plains (GRN) 260',
    'Island (GRN) 261',
    'Swamp (GRN) 262',
    'Mountain (GRN) 263',
    'Forest (GRN) 264',
    'Swamp (DAR) 258',
    'Plains (RNA) 260',
    'Island (RNA) 261',
    'Swamp (RNA) 262',
    'Mountain (RNA) 263',
    'Forest (RNA) 264',
    'Forest (DAR) 266',
    'Island (DAR) 254',
]

to_pt = {
    'Plains': 'Planície',
    'Forest': 'Floresta',
    'Island': 'Ilha',
    'Swamp': 'Pântano',
    'Mountain': 'Montanha',
}
to_en = {pt: en for en, pt in to_pt.items()}

lands_per_color = collections.defaultdict(list)
for land in all_lands:
    land = land.split(None, 1)
    lands_per_color[land[0]].append(land[1])



class Shuffle:
    def __init__(self, root):
        self.root = root
        root.geometry('500x500')
        root.title('Land Shuffler')
        self._create_layout()

    def _create_layout(self):
        frame = t.Frame(self.root)
        frame.pack()
        
        self.text = t.Text(frame)
        self.text.pack()
        button = t.Button(frame, text="Shuffle Lands!", command=self.convert)
        button.pack()

    def convert(self):
        self.land_iterators = {}
        for color, lands in lands_per_color.items():
            random.shuffle(lands)
            self.land_iterators[color] = itertools.cycle(lands)
        translate = False
        text = self.text.get(1.0, t.END)
        new_text = []
        lands_to_add = collections.defaultdict(int)
        for line in text.splitlines():
            number, _sep, land = line.partition(' ')
            color, _ign, rest = land.partition(' ')
            if color not in lands_per_color and color in to_en:
                color = to_en[color]
                translate = True

            if _sep and color in lands_per_color:
                lands_to_add[color] += int(number)
            else:
                if not line.strip(): #add lands before sideboard
                    self._add_lands(new_text, lands_to_add, translate)
                    lands_to_add.clear()
                new_text.append(line + '\n')
        self._add_lands(new_text, lands_to_add, translate)
        self.text.delete(1.0, t.END)
        self.root.update()
        self.text.insert(t.END, ''.join(new_text).rstrip())
        self.root.update()
        self.text.tag_add("sel","1.0","end")
        self.root.update()
        self.text.event_generate("<<Copy>>")

    def _add_lands(self, destination, lands, translate=False):
        for color, number in lands.items():
            lands_per_type = collections.Counter(land_type
                for land_type in itertools.islice(
                    self.land_iterators[color], number))
            for land_type, number in lands_per_type.items():
                destination.append('{} {} {}\n'.format(number,
                    to_pt[color] if translate else color, 
                    land_type)
                )
        

    def run(self):
        self.root.mainloop()

if __name__ == '__main__':
    root = t.Tk()
    app = Shuffle(root)
    app.run()
